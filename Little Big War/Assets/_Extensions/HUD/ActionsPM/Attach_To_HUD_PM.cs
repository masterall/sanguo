//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
//
//-----------------------------------------------------------------------------

using UnityEngine;
using HutongGames.PlayMaker;
using CrenoLab.UniCAE.Util;

[ActionCategory("UniGameKit_HUD")]
public class Attach_To_HUD_PM : FsmStateAction
{
    public FsmGameObject prefab;
    public FsmGameObject target;
    public FsmGameObject store;

	// Code that runs on entering the state.
	public override void OnEnter()
	{
        Do();
        Finish();
	}

	// Code that runs every frame.
	public override void OnUpdate()
	{
       
	}

	public override void OnFixedUpdate()
	{
		
	}

	public override void OnLateUpdate()
	{
		
	}

	// Code that runs when exiting the state.
	public override void OnExit()
	{
		
	}

	// Perform custom error checking here.
	public override string ErrorCheck()
	{
		// Return an error string or null if no error.
		
		return null;
	}

    private void Do()
    {
        if (null == prefab || null == target)
            return;

        HUDRoot root = CAEMono<HUDRoot>.Obj;
        if (null == root)
            return;

        GameObject child = NGUITools.AddChild(root.gameObject, prefab.Value);
        child.AddComponent<UIFollowTarget>().target = target.Value.transform;
        store.Value = child;
        
    }

}
