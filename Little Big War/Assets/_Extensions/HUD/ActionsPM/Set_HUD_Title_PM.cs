//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
//
//-----------------------------------------------------------------------------

using UnityEngine;
using HutongGames.PlayMaker;
using CrenoLab.UniCAE.Util;

[ActionCategory("UniGameKit_HUD")]
public class Set_HUD_Title_PM : FsmStateAction
{
    public FsmOwnerDefault text;
    public FsmString content;
    public Color color = Color.white;
    public bool everyFrame = false;

	// Code that runs on entering the state.
	public override void OnEnter()
	{
        Do();
        if (!everyFrame)
            Finish();
	}

	// Code that runs every frame.
	public override void OnUpdate()
	{
        Do();
	}

	public override void OnFixedUpdate()
	{
		
	}

	public override void OnLateUpdate()
	{
		
	}

	// Code that runs when exiting the state.
	public override void OnExit()
	{
		
	}

	// Perform custom error checking here.
	public override string ErrorCheck()
	{
		// Return an error string or null if no error.
		
		return null;
	}

    private void Do()
    {
        var go = Fsm.GetOwnerDefaultTarget(text);
        if (null == go)
            return;

        HUDTitle cpt = go.GetComponent<HUDTitle>();
        if (null == cpt)
            return;

        cpt.Use(content.Value, color);
        
    }

}
