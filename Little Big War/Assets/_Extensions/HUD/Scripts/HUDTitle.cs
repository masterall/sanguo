﻿//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
// History :
// - From HUDText.cs
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// HUD text creates temporary on-screen text entries that are perfect for damage, effects, and messages.
/// </summary>

public class HUDTitle : MonoBehaviour
{
    protected class Entry
    {
        public UILabel label;		// Label on the game object
    }

    /// <summary>
    /// Font that will be used to create labels.
    /// </summary>

    public UIFont font;

    /// <summary>
    /// Effect applied to the text.
    /// </summary>

    public UILabel.Effect effect = UILabel.Effect.None;

    public float scale = 1.0f;

    private Entry entry;

    /// <summary>
    /// Create a new entry, reusing an old entry if necessary.
    /// </summary>

    Entry Create()
    {
        // New entry
        Entry ne = new Entry();
        ne.label = NGUITools.AddWidget<UILabel>(gameObject);
        ne.label.name = "label";
        ne.label.effectStyle = effect;
        ne.label.font = font;

        // Make it small so that it's invisible to start with
        ne.label.cachedTransform.localScale = new Vector3(font.size* scale, font.size * scale, 1f);

        ne.label.cachedTransform.localPosition = new Vector3(0f, 0f, 0f);
        return ne;
    }

    /// <summary>
    /// Delete the specified entry, adding it to the unused list.
    /// </summary>

    void Delete(Entry ent)
    {
        NGUITools.SetActive(ent.label.gameObject, false);
    }

    /// <summary>
    /// Add a new scrolling text entry.
    /// </summary>

    public void Use(object obj, Color c)
    {
        if (!enabled) return;

        // Create a new entry
        entry = Create();
        entry.label.color = c;
        entry.label.text = obj.ToString();
    }

    /// <summary>
    /// Disable all labels when this script gets disabled.
    /// </summary>

    void OnDisable()
    {

    }

    /// <summary>
    /// Update the position of all labels, as well as update their size and alpha.
    /// </summary>

    void Update()
    {
        
    }
}
