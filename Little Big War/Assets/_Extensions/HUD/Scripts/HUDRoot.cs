﻿//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
//
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using CrenoLab.UniCAE.Util;

public class HUDRoot : MonoBehaviour
{
    void Awake()
    {
        CAEMono<HUDRoot>.Obj = this;
    }
}
