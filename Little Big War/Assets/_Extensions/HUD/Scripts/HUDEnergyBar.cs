﻿//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
// History :
// - From HUDText.cs
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// HUD text creates temporary on-screen text entries that are perfect for damage, effects, and messages.
/// </summary>

public class HUDEnergyBar : MonoBehaviour
{

    public UISprite background;

    private bool adjust = false;
    void Update()
    {
        if (adjust || null == background)
            return;

        Vector3 pos = background.transform.parent.transform.localPosition;
        pos.x = -(background.gameObject.transform.localScale.x) / 2;
        background.transform.parent.transform.localPosition = pos;
        adjust = true;
    }
}
