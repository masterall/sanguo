﻿//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
//
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class NGUIClickUnitObj : MonoBehaviour {

    public delegate void OnClickDelegate();
    public OnClickDelegate OnClickCallBack;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnClick()
    {
        if (null != OnClickCallBack)
            OnClickCallBack();
    }
}
