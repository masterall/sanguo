﻿//-----------------------------------------------------------------------------
// 
// - UniCAE -
//
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/09/16
//
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.IO;
using CrenoLab.UniCAE.Logger;

using CrenoLab.UniCAE.Storage;

public class CAESQLObj : MonoBehaviour {

    public enum PathType { STREAMING_ASSETS }
    public PathType path = PathType.STREAMING_ASSETS;
    public string db;

    private string dbPath;

	// Use this for initialization
	void Start () {
        if (string.IsNullOrEmpty(db))
            return;

        StartCoroutine(InitializeDB());
	}

    void OnDestroy()
    {
        CAESQL.Close();
    }


    private IEnumerator InitializeDB()
    {
        byte[] bytes = null;

#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
        dbPath = "file://" + Application.streamingAssetsPath + "/" + db;
        WWW www = new WWW(dbPath);
        yield return www;
        bytes = www.bytes;
#elif UNITY_WEBPLAYER
			dbPath = "StreamingAssets/" + db;
            WWW www = new WWW(dbPath);
            yield return www;
            bytes = www.bytes;
#elif UNITY_IPHONE
			dbPath = Application.dataPath + "/Raw/" + db;
            try
                {
                    using (FileStream fs = new FileStream(dbPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        bytes = new byte[fs.Length];
                        fs.Read(bytes, 0, (int)fs.Length);
                    }
                }
                catch (System.Exception e)
                {
                    throw e;
                }
            yield return 0;
#elif UNITY_ANDROID
			dbPath = Application.streamingAssetsPath + "/" + db;
            WWW www = new WWW(dbPath);
            yield return www;
            bytes = www.bytes;
#endif

        this.LogDebug("DB path: " + dbPath);
        CAESQL.Open(bytes);
    }
}
