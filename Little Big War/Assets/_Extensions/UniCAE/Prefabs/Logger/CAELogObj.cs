//-----------------------------------------------------------------------------
// 
// - UniCAE -
//
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/07/17
//
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using CrenoLab.UniCAE.Logger;

[AddComponentMenu("UniCAE/Log")]
public class CAELogObj : MonoBehaviour
{
	public CAELog.LogLevel level = CAELog.LogLevel.ALL;
    public enum LogType { CONSOLE };
    public LogType logType = LogType.CONSOLE;

	public void Awake()
	{
		CAELog.SetLevel(level);
        if (LogType.CONSOLE == logType)
        {
            CAELog.SetImplement(new CAEConsoleLog());
            this.LogDebug("Use Console Log");
        }
	}

}
